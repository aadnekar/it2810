import colors from '../utils/colors.js';
import { randomInt } from '../utils/random.js';

export class SquareSVG {
  constructor(parent, x, y, length, dx, dy) {
    this.parent = parent;
    this.shape = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    this.x = x;
    this.y = y;
    this.length = length;
    this.dx = dx;
    this.dy = dy;
    this.color = colors[randomInt(0, 3)];
    this.minRadius = 10;
    this.maxRadius = 60;
    this.parent.append(this.shape);

    this.draw = () => {
      this.shape.setAttributeNS(null, 'x', this.x);
      this.shape.setAttributeNS(null, 'y', this.y);
      this.shape.setAttributeNS(null, 'width', this.length);
      this.shape.setAttributeNS(null, 'height', this.length);
      this.shape.setAttributeNS(null, 'fill', this.color);
    };

    this.update = () => {
      if (
        this.x + this.length >
          parseInt(window.getComputedStyle(this.parent).width) ||
        this.x - this.length < 0
      ) {
        this.dx = -this.dx;
      }
      if (
        this.y + this.length >
          parseInt(window.getComputedStyle(this.parent).height) ||
        this.y - this.length < 0
      ) {
        this.dy = -this.dy;
      }
      this.x += this.dx;
      this.y += this.dy;
    };

    this.hide = () => {
      this.shape.setAttributeNS(null, 'visibility', 'hidden');
    };

    this.show = () => {
      this.shape.setAttributeNS(null, 'visibility', 'visible');
    };
  }
}
