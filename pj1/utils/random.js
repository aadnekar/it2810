export const randomInt = (min, max) => {
  return Math.floor(Math.random() * (max + 1) + min);
};

export const randomFloat = (min, max) => {
  return Math.random() * max + min;
};

export const randomX = (min, max) => {
  return randomInt(min, max);
};

export const randomY = (min, max) => {
  return randomInt(min, max);
};
