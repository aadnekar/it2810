import colors from '../utils/colors.js';
import { randomInt } from '../utils/random.js';

export class Circle {
  constructor(x, y, r, dx, dy) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.dx = dx;
    this.dy = dy;
    this.color = colors[randomInt(0, 3)];
    this.minRadius = 10;
    this.maxRadius = 60;

    this.draw = (context) => {
      context.beginPath();
      context.arc(this.x, this.y, this.r, 0, Math.PI * 2, false);
      context.fillStyle = this.color;
      context.stroke();
      context.fill();
    };

    this.update = (mouse) => {
      if (this.x + this.r > canvas.width || this.x - this.r < 0) {
        this.dx = -this.dx;
      }
      if (this.y + this.r > canvas.height || this.y - this.r < 0) {
        this.dy = -this.dy;
      }
      this.x += this.dx;
      this.y += this.dy;

      if (this.closeToCursor(mouse) && this.r < this.maxRadius) {
        this.r += 10;
      } else if (this.r > this.minRadius) {
        this.r -= 1;
      }
    };

    this.closeToCursor = (mouse) => {
      return (
        mouse.x - this.x < 50 &&
        mouse.x - this.x > -50 &&
        mouse.y - this.y < 50 &&
        mouse.y - this.y > -50
      );
    };
  }
}
