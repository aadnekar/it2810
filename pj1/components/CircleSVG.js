import colors from '../utils/colors.js';
import { randomInt } from '../utils/random.js';

export class CircleSVG {
  constructor(parent, x, y, r, dx, dy) {
    this.parent = parent;
    this.shape = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'circle'
    );
    this.x = x;
    this.y = y;
    this.r = r;
    this.dx = dx;
    this.dy = dy;
    this.color = colors[randomInt(0, 3)];
    this.minRadius = 10;
    this.maxRadius = 60;
    this.parent.append(this.shape);

    this.draw = () => {
      this.shape.setAttributeNS(null, 'cx', this.x);
      this.shape.setAttributeNS(null, 'cy', this.y);
      this.shape.setAttributeNS(null, 'r', this.r);
      this.shape.setAttributeNS(null, 'fill', this.color);
    };

    this.update = (mouse) => {
      if (
        this.x + this.r >
          parseInt(window.getComputedStyle(this.parent).width) ||
        this.x - this.r < 0
      ) {
        this.dx = -this.dx;
      }
      if (
        this.y + this.r >
          parseInt(window.getComputedStyle(this.parent).height) ||
        this.y - this.r < 0
      ) {
        this.dy = -this.dy;
      }
      this.x += this.dx;
      this.y += this.dy;

      if (this.closeToCursor(mouse) && this.r < this.maxRadius) {
        this.r += 10;
      } else if (this.r > this.minRadius) {
        this.r -= 1;
      }
    };

    this.closeToCursor = (mouse) => {
      return (
        mouse.x - this.x < 50 &&
        mouse.x - this.x > -50 &&
        mouse.y - this.y < 50 &&
        mouse.y - this.y > -50
      );
    };

    this.hide = () => {
      this.shape.setAttributeNS(null, 'visibility', 'hidden');
    };

    this.show = () => {
      this.shape.setAttributeNS(null, 'visibility', 'visible');
    };
  }
}
