const docBtn = document.getElementById('doc-btn');
const doc = document.getElementById('doc');

export const instantiateDocumentationVisbility = () => {
  window.localStorage.getItem('isDocVisible') === 'true'
    ? setDocumentationVisibility('block')
    : setDocumentationVisibility('none');

  docBtn.addEventListener('click', (e) => {
    e.preventDefault;
    setDocumentationVisibility(toggleDocumentationVisibility(doc));
  });
};

const setDocumentationVisibility = (visibility) => {
  doc.style.display = visibility;
};

export const toggleDocumentationVisibility = (elem) => {
  if (window.getComputedStyle(elem).display !== 'none') {
    window.localStorage.setItem('isDocVisible', 'false');
    return 'none';
  }
  window.localStorage.setItem('isDocVisible', 'true');
  return 'block';
};
