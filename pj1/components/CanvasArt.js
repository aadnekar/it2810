import { Mouse } from './Mouse.js';
import { Circle } from './Circle.js';
import { Square } from './Square.js';
import { randomInt, randomFloat, randomX, randomY } from './../utils/random.js';

const artWork = document.getElementById('canvas-art');
const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d');
const circles = [];
const squares = [];
let req;

const mouse = new Mouse();
const NUMBER_OF_SHAPES = 150;

window.addEventListener('mousemove', (event) => {
  const artWorkOffset = canvas.getBoundingClientRect();
  mouse.update(event.x - artWorkOffset.left, event.y - artWorkOffset.top);
});

window.addEventListener('resize', () => {
  setCanvasSize();
});

const setCanvasSize = () => {
  canvas.width = parseInt(window.getComputedStyle(artWork).width);
  canvas.height = parseInt(window.getComputedStyle(artWork).height);
};

const animateCircles = () => {
  req = requestAnimationFrame(animateCircles);

  context.clearRect(0, 0, canvas.width, canvas.height);

  circles.forEach((circle) => {
    circle.draw(context);
    circle.update(mouse);
  });
};

const animateSquares = () => {
  req = requestAnimationFrame(animateSquares);
  context.clearRect(0, 0, canvas.width, canvas.height);
  squares.forEach((square) => {
    square.draw(context);
    square.update();
  });
};

export const initiateCanvasArt = () => {
  setCanvasSize();

  $(canvas).mouseenter(() => {
    window.cancelAnimationFrame(req);
    animateCircles();
  });
  $(canvas).mouseleave(() => {
    window.cancelAnimationFrame(req);
    animateSquares();
  });

  for (let i = 0; i < NUMBER_OF_SHAPES; i++) {
    circles.push(
      new Circle(
        randomX(10, canvas.width - 30),
        randomY(10, canvas.width - 30),
        randomInt(2, 10),
        randomFloat(0.1, 2),
        randomInt(0.1, 2)
      )
    );
    squares.push(
      new Square(
        randomX(10, canvas.width - 30),
        randomY(10, canvas.width - 30),
        randomInt(1, 10),
        randomFloat(0.1, 2),
        randomFloat(0.1, 2)
      )
    );
  }

  animateSquares();
};
