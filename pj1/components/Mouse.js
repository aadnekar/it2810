export class Mouse {
  constructor() {
    this.x = undefined;
    this.y = undefined;

    this.update = (x, y) => {
      this.x = x;
      this.y = y;
    };
  }
}
