# Project 1: My abstract image

## Practical

- Submission date: **Friday 28/8**
- Three peer reviews within: **Friday 4/9**
- Individual tasks

## Description

Two abstract art works. One based only on SVGs and the other based on HTML 5 canvas and the canvas api.

## Requirements

1. Some information about the artist _(Don't use a personal name)_
2. The images (art work) and title:
   - Has to be of some complexity, but not bigger than may be coded manually. Minimum five elements with a bit of variation in both type and color.
   - The art work has to be interactive in some way. May change color on hover.
3. A button on the bottom of the page to toggle documentation.

### Techonology requirements

- Use SVG for one image
- Use HTML5 canvas and canvas api for drawing one image
- Use CSS-grid and/or CSS flexbox for layout
- Use jQuery library

### Testing requirements

- The webpage should act similarily on different browsers for pc
- Test in minimum two browsers

### Documentation requirements

- Describe how CSS-grid and Flexbox has been used
- Describe how HTML canvas and SVG has been used
- Describe how jQuery has been used
- Describe how cross browser testing has been performed
- List the must important sources of information that has been used
- Remember to site sources if code has been copied
