import colors from '../utils/colors.js';
import { randomInt } from '../utils/random.js';

export class Square {
  constructor(x, y, length, dx, dy) {
    this.x = x;
    this.y = y;
    this.length = length;
    this.dx = dx;
    this.dy = dy;
    this.color = colors[randomInt(0, 3)];
    this.minLength = 10;
    this.maxLength = 60;

    this.draw = (context) => {
      context.beginPath();
      context.rect(this.x, this.y, this.length, this.length);
      context.fillStyle = this.color;
      context.stroke();
      context.fill();
    };

    this.update = () => {
      if (this.x + this.length > canvas.width || this.x - this.length < 0) {
        this.dx = -this.dx;
      }
      if (this.y + this.length > canvas.height || this.y - this.length < 0) {
        this.dy = -this.dy;
      }
      this.x += this.dx;
      this.y += this.dy;
    };
  }
}
