import { Mouse } from './Mouse.js';
import { CircleSVG } from './CircleSVG.js';
import { SquareSVG } from './SquareSVG.js';
import { randomInt, randomFloat, randomX, randomY } from './../utils/random.js';

const svgArt = document.getElementById('svg-art');
const circles = [];
const squares = [];
let req;

const mouse = new Mouse();
const NUMBER_OF_SHAPES = 150;

const setSVGArtSize = () => {
  const artWork = svgArt.parentElement;
  svgArt.setAttribute(
    'width',
    parseInt(window.getComputedStyle(artWork).width)
  );
  svgArt.setAttribute(
    'height',
    parseInt(window.getComputedStyle(artWork).height)
  );
  let titleTag = artWork.querySelector('.title-tag');
  titleTag.style.marginTop = `${
    parseInt(window.getComputedStyle(artWork).height) + 40
  }px`;
};

export const instantiateSVGArt = () => {
  setSVGArtSize();

  svgArt.addEventListener('mousemove', (event) => {
    const artWorkOffset = svgArt.getBoundingClientRect();
    mouse.update(event.x - artWorkOffset.left, event.y - artWorkOffset.top);
  });

  window.addEventListener('resize', () => {
    setSVGArtSize();
  });

  $(svgArt).mouseenter(() => {
    window.cancelAnimationFrame(req);
    hideAllElements(squares);
    showAllElements(circles);

    animateCircles();
  });
  $(svgArt).mouseleave(() => {
    window.cancelAnimationFrame(req);
    hideAllElements(circles);
    showAllElements(squares);

    animateSquares();
  });

  for (let i = 0; i < NUMBER_OF_SHAPES; i++) {
    circles.push(
      new CircleSVG(
        svgArt,
        randomX(10, parseInt(window.getComputedStyle(svgArt).width) - 30),
        randomY(10, parseInt(window.getComputedStyle(svgArt).height) - 30),
        randomInt(10, 20),
        randomFloat(-2, 2),
        randomInt(-2, 2)
      )
    );

    squares.push(
      new SquareSVG(
        svgArt,
        randomX(10, parseInt(window.getComputedStyle(svgArt).width) - 30),
        randomY(10, parseInt(window.getComputedStyle(svgArt).height) - 30),
        randomInt(5, 15),
        randomFloat(-2, 2),
        randomFloat(-2, 2)
      )
    );
  }

  const hideAllElements = (elements) => {
    elements.forEach((elem) => {
      elem.hide();
    });
  };

  const showAllElements = (elements) => {
    elements.forEach((elem) => {
      elem.show();
    });
  };

  const animateCircles = () => {
    req = requestAnimationFrame(animateCircles);

    circles.forEach((circle) => {
      circle.draw();
      circle.update(mouse);
    });
  };

  const animateSquares = () => {
    req = requestAnimationFrame(animateSquares);

    squares.forEach((square) => {
      square.draw();
      square.update();
    });
  };

  animateSquares();
};
