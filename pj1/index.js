import { instantiateSVGArt } from './components/SVGArt.js';
import { initiateCanvasArt } from './components/CanvasArt.js';
import { instantiateDocumentationVisbility } from './components/Documentation.js';

instantiateDocumentationVisbility();
instantiateSVGArt();
initiateCanvasArt();
